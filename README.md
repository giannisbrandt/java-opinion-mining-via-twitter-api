General Information:

This project collects a specified amount of data (tweets) via Twitter API for the top 5 trending topics on USA. The data are stored in MongoDB and 
are processed (removal of stop words, symbols ,numbers ,transformation to lower case) to make the analysis more efficient. 
Then, through sentiment analysis to each tweet the opinion of users to each topic can be determined and the appropriate visual statistics are produced.

Main.java:
For the collection of the tweets for each one of the 5 trending topics twitter's API is used using the code 23424977 (USA).
In this class, MongoDB is initialized with different collections for each topic.
For each of the 5 trending topics:

1500 tweets are collected (retweets are not collected) and stored in the corresponding
collection.
Every tweet is processed (removal of stopwords, uppercase letters,numbers,symbols) to transform
the data in the appropriate form. The processed data are being stored back in MongoDB updating
the previous entries.
For each tweet we collect further information about the users which will help to produce a CDF
diagram.
The collected data are also stored in two HashMaps --before and after the removal of the
stop words.These HashMaps are used to produce visual statistics such as Pie Charts,Zipf
Diagrams, CDF diagrams,Line Diagrams.
Using the class SentimentAnalysis.java the sentiment of each user's tweet is determined and
stored.
Using the avg method of MongoDB we determine the average sentiment score of the user's around
each topic.

SentimentAnalysis.java:
This class is used to determine the sentiment of a user's tweet (negative,neutral,positive).
This is achieved using a machine learning API which can be found here :http://text-processing.com/docs/sentiment.html.
For each user's tweet we acquire the result of the sentiment analysis which is stored in the corresponding MongoDb collection.

Stopwords.java:
This class reads and stores a file stopwords.txt which contains a large amount of stopwords.The stored stopwords must be removed for every tweet to make the 
sentiment analysis more efficient.

TextEdit.java:
This class is responsible of the removal of stop words, symbols ,numbers ,transformation to lower case of every tweet.
WordCount.java:
This class is used to order the HashMap based on it's values. It is used to determine the 50 most famous words of the tweets around each topic which assists the creation of the Histograms.
Histogram.java LineDiagram.java PieChart.java


Histogram.java receives a HashMap with the 50 most famous words that is used on the tweets of
every topic and then produces a histogram.


LineDiagram.java is used to produce the zipf and cdf diagrams.


PieChart.java receives three values which work as counters for the dominant sentiment of each
topic and produces the corresponding pie charts.