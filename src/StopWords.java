import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/*
 * This class is used to store stop words of a given file filled with stop words.
 * The stop words are stored in an ArrayList and then used to generate the asked histograms.
 * 
 */
public class StopWords {

	private ArrayList<String> list;
	
	public StopWords() {
		Scanner s;
		try {
			s = new Scanner(new File("stopwords.txt"));
			list = new ArrayList<String>();
			while (s.hasNext()){
			    list.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.out.println("File stopwords.txt not found.Exception: " + e);
		}
		
	}
	
	public ArrayList<String> getStopwords(){
		return list;
	}
	
	public String removeStopwords(String text){
		StringTokenizer tokens = new StringTokenizer(text);
		String toReturn = "";
		while (tokens.hasMoreTokens()){
			String word = tokens.nextToken();
			if (!list.contains(word)){
				toReturn = toReturn + word + " ";
			}
		}
		return toReturn;
	}

}
