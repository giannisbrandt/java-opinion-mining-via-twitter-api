
import java.io.File;
import java.io.IOException;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/*
 * This class is used to generate the asked PieCharts.
 * Variables countNeg,countPos,countNeutral are calculated within Main class and they count the superior 
 * sentiment of every tweet.
 * The PieChart is generated using JFreeChart library.
 */
public class PieChart extends ApplicationFrame {
	
	private static int countPos,countNeg,countNeutral;
    private static String title1;
    
   public PieChart( String title ,int countNeg ,int countPos ,int countNeutral) {
      super( title );
      this.countNeg = countNeg;
      this.countPos = countPos;
      this.countNeutral = countNeutral;
      this.title1=title;
      setContentPane(createDemoPanel(title));
      
   }
   
   public static JPanel createDemoPanel(String title) {
	      title1 = title;
	      PieDataset dataset = createDataset(title);
	      JFreeChart chart = createChart(dataset );  
	      return new ChartPanel( chart ); 
	   }
   
   private static PieDataset createDataset(String title) {
      DefaultPieDataset dataset = new DefaultPieDataset( );
      dataset.setValue( "Positive" , new Integer(countPos) );  
      dataset.setValue( "Negative" , new Integer(countNeg) );   
      dataset.setValue( "Neutral" , countNeutral);     
      return dataset;         
   }
   
   private static JFreeChart createChart(PieDataset dataset) {
      JFreeChart chart = ChartFactory.createPieChart(      
         "Pie chart",   // chart title 
         dataset,       // data    
         true,   
         true, 
         false);

      try {
          ChartUtilities.saveChartAsPNG(new File(title1 + ".png"), chart, 500, 270);
          } catch (IOException e) {}
           
      return chart;
   }
}
