import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;


public class LineDiagram extends ApplicationFrame{

	/**
	 * @param args
	 */
	public LineDiagram(HashMap<Double,Double> cdfData,String title){
		super(title);
	    final XYSeries points = new XYSeries("Data from Twitter");
	    Iterator it = cdfData.entrySet().iterator();
	    while(it.hasNext()){
	    	Map.Entry entry = (Map.Entry) it.next();
	    	double key = (double)entry.getKey();
	    	double value = (double) entry.getValue();
	    	points.add(key,value);
	    }
	    
		final XYSeriesCollection data = new XYSeriesCollection(points);
		
	    final JFreeChart chart = ChartFactory.createXYLineChart(
	        "Diagramm",
	        "X", 
	        "Y", 
	        data,
	        PlotOrientation.VERTICAL,
	        true,
	        false,
	        false
	    );

	    try {
	        ChartUtilities.saveChartAsPNG(new File(title), chart, 500, 270);
	        } catch (IOException e) {}
	         }
	}
