
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.jfree.chart.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.*;
import org.jfree.chart.plot.PlotOrientation;

 /*
  * This class is used to generate the appropriate histograms. 
  *  It accepts a sorted HashMap(descending order depending on the values of the HashMap) and creates
  *  a histogram -using JFreeChart library- for the  50 most used words.
  *  
  */

 public class Histogram {
	 private String title , pngTitle;
	 
	 public Histogram(String title,String pngTitle){
		 this.title = title;
		 this.pngTitle = pngTitle;
	 }
	 
	 
       public void start(HashMap<String,Double> data) {

       HistogramDataset dataset = new HistogramDataset();
       DefaultCategoryDataset db = new DefaultCategoryDataset();
       
       Set set2 = data.entrySet();
	      Iterator iterator2 = set2.iterator();
	     for (int i=0;i<50;i++){
	    	 if (iterator2.hasNext()){
	           Map.Entry me2 = (Map.Entry)iterator2.next();
	           String key = (String) me2.getKey();
	           double value = (double) me2.getValue();
	           db.setValue(value, "", key);
	    	 }
	     }

       JFreeChart chart = ChartFactory.createBarChart(
               title, "Word", "Word Count",
               db, PlotOrientation.HORIZONTAL, false, true, false);

       int width = 1500;
       int height = 1500; 
        try {
        ChartUtilities.saveChartAsPNG(new File(pngTitle), chart, width, height);
        } catch (IOException e) {}
         }
   }

