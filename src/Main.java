import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.JFrame;

import org.bson.BasicBSONObject;
import org.bson.Document;
import org.jfree.ui.RefineryUtilities;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.util.JSON;

import twitter4j.GeoLocation;
import twitter4j.Location;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.api.TrendsResources;
import twitter4j.conf.ConfigurationBuilder;

/*
 * Connection with twitterAPI and MongoDb.
 * Through twitterAPI a number of ~1500 tweets is being retrieved and stored in MongoDb (retweets are removed).
 * Text-processing in every tweet removing numbers,symbols and transforming text to lower case.
 * Sentiment analysis of every tweet.
 * Creation of the appropriate histograms and diagrams.
 */
public class Main implements TrendsResources{

	/**
	 * @param args
	 * @throws TwitterException 
	 * @throws UnknownHostException 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws TwitterException{
		
		int numberOfTweets = 1500;  //total number of tweets which are expected to be collected for each topic.
		
		// setting auth parameters for twitter API access 
		final String TWITTER_CONSUMER_KEY = "N9i5ogvHwHzf80UhiajoQxntD";
		final String TWITTER_SECRET_KEY = "brGTogTPPXpR9ASwXdPfbH5SoRtJzy7TC2bCiA7va2Lkf48dEr";
		final String TWITTER_ACCESS_TOKEN = "922878454101659648-4aO6frvJDfNWSIUVKiYqNlRWKnr2rZY";
		final String TWITTER_ACCESS_TOKEN_SECRET = "OAzZxsjQfSrpVpfFuXvgN7W3wd6z64nH0nBJZBRHoyQkX";

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		    .setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
		    .setOAuthConsumerSecret(TWITTER_SECRET_KEY)
		    .setOAuthAccessToken(TWITTER_ACCESS_TOKEN)
		    .setOAuthAccessTokenSecret(TWITTER_ACCESS_TOKEN_SECRET)
		    .setJSONStoreEnabled(true);
		
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		
		Trends trends = twitter.getPlaceTrends(23424977);  //get top trends for United States
		Trend[] trend = trends.getTrends();
		
		//current top trends of twitter
		System.out.println("Current trends on twitter for USA:");
		for (int i=0;i<5;i++){
			System.out.println(i + "." + trend[i].getName()); //print trend names
		}
		
		int countNeg=0,countPos=0,countNeutral=0;  //counters for sentimental analysis

		//establishing connection with mongodb in port 27017.
		try{	
		MongoClient mongo = new MongoClient("localhost",27017);
		DB db = mongo.getDB("tweetsD");
		DBCollection table = db.getCollection("trend1");
		DBCollection table2 = db.getCollection("trend2");
		DBCollection table3 = db.getCollection("trend3");
		DBCollection table4 = db.getCollection("trend4");
		DBCollection table5 = db.getCollection("trend5");

		//get tweets for every trend
		String text,newText,counter="";
		String[] results = new String[4];
		HashMap<Double,Double> cdfData = new HashMap<>();
	    
		for (int i=0;i<5;i++){
			
			countNeg=0;countPos=0;countNeutral=0;	//setted to 0 to count the sentiment for the next trend
			int sum=0;
			ArrayList<Status> twCount = new ArrayList<>(); //arraylist used to retrieve the appropriate amount of tweets
			
			Query query = new Query(trend[i].getName() + "-filter:retweets");
			QueryResult result = twitter.search(query);
			
			HashMap<String,Double> wordCountMap = new HashMap<>();
		    HashMap<String,Double> wordCountNoStop = new HashMap<>();

		//iterating through all tweets of the collection until we get the expected number of tweets.
		  while (sum< numberOfTweets){
		    	twCount.addAll(result.getTweets());
		    	sum += twCount.size();
		  }
		  //iterating through the stored tweets
			 for (Status statusre : twCount) {
				if (statusre.getLang().equals("en")){      //get only tweets which are written in english.
					//sum+=1;
				newText="";
				text= statusre.getText(); 					//get the text from every tweet.
				TextEdit sth = new TextEdit();
				newText=sth.textTokenize(text);				//tokenize the text of the tweets,remove symbols,
															//transform to lower-case.
				
				//getting data to produce cdf diagram and storing them to the hashmap cdfData.
				double x = (double) statusre.getUser().getFollowersCount();
				double y = (double) statusre.getUser().getFriendsCount();
				cdfData.put(x,y);
				
				SentimentAnalysis sA = new SentimentAnalysis();
				try{
				results = sA.analyze(newText);		 //post request to get sentimental analysis for the tweets.
				}catch(Exception e){
					e.printStackTrace();
					System.out.println("---DAILY REQUEST LIMIT IN SENTIMENT API EXCEEDED---");
				}
				String json = TwitterObjectFactory.getRawJSON(statusre);
				DBObject dbo = (DBObject) JSON.parse(json);

				wordCountMap = storeTextWithStopwords(wordCountMap,newText); //storing all words after symbols and numbers have been removed.
				wordCountNoStop = storeTextNoStopwords(wordCountNoStop,newText); //storing all words except stopwords, after symbols and numbers have removed.

				StopWords stW = new StopWords();
				newText = stW.removeStopwords(newText);		//removing stopwords.
				
				//Query that is used to update the text of the tweets with the edited text.
				BasicDBObject newDocument = new BasicDBObject();
				newDocument.append("$set", new BasicDBObject().append("text", newText));
				BasicDBObject searchQuery = new BasicDBObject().append("text", dbo.get("text"));
				
				//1.saving tweets from different trends to different collections
				//2.inserting positive,negative,neutral,label fields in our database
				//3.counting labels to create the pie charts for each trend respectively
				switch(i){
					case 0: table.insert(dbo);
							table.update(searchQuery, newDocument); //updating the text
							mongo.getDB("tweetsD").getCollection("trend1").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("negative",Double.valueOf(results[0]))));
							mongo.getDB("tweetsD").getCollection("trend1").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("neutral", Double.valueOf(results[1]))));
							mongo.getDB("tweetsD").getCollection("trend1").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("positive",Double.valueOf(results[2]))));
							mongo.getDB("tweetsD").getCollection("trend1").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("label", results[3])));
							countNeg = countLabels("neg",table);
							countPos = countLabels("pos",table);
							countNeutral = countLabels("neutral",table);
							break;
					case 1: table2.insert(dbo);
							table2.update(searchQuery, newDocument);
							mongo.getDB("tweetsD").getCollection("trend2").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("negative",Double.valueOf(results[0]))));
							mongo.getDB("tweetsD").getCollection("trend2").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("neutral", Double.valueOf(results[1]))));
							mongo.getDB("tweetsD").getCollection("trend2").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("positive",Double.valueOf(results[2]))));
							mongo.getDB("tweetsD").getCollection("trend2").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("label", results[3])));
							countNeg = countLabels("neg",table2);
							countPos = countLabels("pos",table2);
							countNeutral = countLabels("neutral",table2);
							break;
					case 2: table3.insert(dbo);
							table3.update(searchQuery, newDocument);
							mongo.getDB("tweetsD").getCollection("trend3").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("negative",Double.valueOf(results[0]))));
							mongo.getDB("tweetsD").getCollection("trend3").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("neutral", Double.valueOf(results[1]))));
							mongo.getDB("tweetsD").getCollection("trend3").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("positive",Double.valueOf(results[2]))));
							mongo.getDB("tweetsD").getCollection("trend3").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("label", results[3])));
							countNeg = countLabels("neg",table3);
							countPos = countLabels("pos",table3);
							countNeutral = countLabels("neutral",table3);
							break;
					case 3: table4.insert(dbo);
							table4.update(searchQuery, newDocument);
							mongo.getDB("tweetsD").getCollection("trend4").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("negative",Double.valueOf(results[0]))));
							mongo.getDB("tweetsD").getCollection("trend4").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("neutral", Double.valueOf(results[1]))));
							mongo.getDB("tweetsD").getCollection("trend4").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("positive",Double.valueOf(results[2]))));
							mongo.getDB("tweetsD").getCollection("trend4").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("label", results[3])));
							countNeg = countLabels("neg",table4);
							countPos = countLabels("pos",table4);
							countNeutral = countLabels("neutral",table4);
							break;
					case 4: table5.insert(dbo);
							table5.update(searchQuery, newDocument);
							mongo.getDB("tweetsD").getCollection("trend5").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("negative",Double.valueOf(results[0]))));
							mongo.getDB("tweetsD").getCollection("trend5").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("neutral", Double.valueOf(results[1]))));
							mongo.getDB("tweetsD").getCollection("trend5").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("positive",Double.valueOf(results[2]))));
							mongo.getDB("tweetsD").getCollection("trend5").update(new BasicDBObject("id", dbo.get("id")),new BasicDBObject("$set", new BasicDBObject("label", results[3])));
							countNeg = countLabels("neg",table5);
							countPos = countLabels("pos",table5);
							countNeutral = countLabels("neutral",table5);
							break;
				}
				
				}
			}
		
			twCount.clear(); //clear arraylist so that next trend's tweets will be counted appropriately.
			
			//sorting saved words in descending order (depending on number of occurences) to print histograms.
			WordCount words = new WordCount();
			wordCountMap = words.sort(wordCountMap);
			wordCountNoStop = words.sort(wordCountNoStop);

			//generating histogram with stopwords.
			Histogram hs = new Histogram(trend[i].getName(),trend[i].getName()+ "WITHstopwords" + ".png");
			hs.start(wordCountMap);
			
			//generating histogram without stopwords.
			Histogram hsNo = new Histogram(trend[i].getName(),trend[i].getName()+ "NOstopwords" + ".png");
			hsNo.start(wordCountNoStop);
		    
		   //generating pie chart for every trend.
			 PieChart chart4 = new PieChart(trend[i].getName(),countNeg,countPos,countNeutral);
			 chart4.setSize(560 , 367);    
		     RefineryUtilities.centerFrameOnScreen(chart4);    
		     
		    //create appropriate zipf data to generate zipf diagram.
		 	HashMap<Double,Double> zipfData = new HashMap<>();
		 	zipfData = createZipfData(zipfData,wordCountMap);
		 	zipfData=words.sort(zipfData);
		 	  
		     //finding the avg sentiment of a user per trend		     
		     DBObject groupFields = new BasicDBObject( "_id", "$id");
		     groupFields.put("averageNeg", new BasicDBObject( "$avg", "$negative"));
		     groupFields.put("averagePos", new BasicDBObject( "$avg", "$positive"));
		     groupFields.put("averageNeutral", new BasicDBObject( "$avg", "$neutral"));
		     
		     DBObject group = new BasicDBObject("$group", groupFields);

		     AggregationOutput output = db.getCollection("twitter").aggregate(group);
		     Iterable<DBObject> list = output.results();

		     //plotting cdf diagram
		     LineDiagram plot = new LineDiagram(cdfData,"CDF diagramm for trend: " + trend[i].getName());
		     plot.setSize(560 , 367);    
		     RefineryUtilities.centerFrameOnScreen(plot);    
		     
		     //plotting zipf diagram
		     LineDiagram plot2 = new LineDiagram(zipfData,"Zipf diagramm for trend: " + trend[i].getName());
		     plot2.setSize(560 , 367);    
		     RefineryUtilities.centerFrameOnScreen(plot2);    
		     
		     //writing the avg sentiment of users in a file.
		     Writer writer = null;
		     try {
		         writer = new BufferedWriter(new OutputStreamWriter(
		               new FileOutputStream("sent_" + trend[i].getName() + ".txt"), "utf-8"));
		         Iterator<DBObject> it = list.iterator();
			     while(it.hasNext()){
			    	 writer.write(it.next().toString());
			     }
		     } catch (IOException ex) {
		       ex.printStackTrace();
		     } finally {
		        try {writer.close();} catch (Exception ex){
		        	ex.printStackTrace();
		        }
		     }

		     System.out.println("Created .txt file for user sentimental score for trend " + trend[i].getName() + " . . .");
		     System.out.println("Next trend . . .");
		     wordCountMap.clear();
		     wordCountNoStop.clear();
		}
		mongo.close();

	  }catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception here: " + e);
	  }
		
		
		
		
}
	
	//method which is used to store all words after symbols and numbers are removed, but including stopwords.
	public static HashMap<String,Double> storeTextWithStopwords(HashMap<String,Double> wordCountMap,String newText){
		
		String tweetText = newText;
		StringTokenizer tokens = new StringTokenizer(tweetText);
		while (tokens.hasMoreTokens()){
			String nextToken = tokens.nextToken();
			if (wordCountMap.containsKey(nextToken)){
				wordCountMap.put(nextToken, wordCountMap.get(nextToken)+1);
			}
			else{
				wordCountMap.put(nextToken, 0.0);
			}
		}
		return wordCountMap;
	}
	
	//method which is used to store all words after symbols and numbers are removed, but WITHOUT stopwords.
	public static HashMap<String,Double> storeTextNoStopwords(HashMap<String,Double> wordCountNoStop,String newText){
		String tweetText = newText;
		StringTokenizer tokens = new StringTokenizer(tweetText);
		
		ArrayList<String> stopWords = new ArrayList<>();
		StopWords sw = new StopWords();
		stopWords = sw.getStopwords();
		
		while (tokens.hasMoreTokens()){
			String nextToken = tokens.nextToken();
			if (wordCountNoStop.containsKey(nextToken)){
				if (!stopWords.contains(nextToken)){
					wordCountNoStop.put(nextToken, wordCountNoStop.get(nextToken)+1);
				}
			}
			else{
				if (!stopWords.contains(nextToken)){
					wordCountNoStop.put(nextToken, 0.0);
				}
			}
		}
		return wordCountNoStop;
	}
	
	//this method is used to generate data that are going to be used to produce the zipf diagrams.
	public static HashMap<Double,Double> createZipfData(HashMap<Double,Double> zipfData,HashMap<String,Double> wordCountMap){
		double c = 1.0; //c is the rank of every word.
	 	double count=2;
		for (Iterator it = wordCountMap.entrySet().iterator(); it.hasNext();) {
	 	     Map.Entry entry = (Map.Entry) it.next();
	 	     double occ = (double) entry.getValue();
	 	     zipfData.put(c, occ);
	 	     c++;
	 		}
		return zipfData;
	}

	public static int countLabels(String category,DBCollection table){
		BasicDBObject query = new BasicDBObject();
		query.put("label", category);
		int counter = table.find(query).count();
		return counter;
	}

	public ResponseList<Location> getAvailableTrends() throws TwitterException {
		// TODO Auto-generated method stub
		return null;
	}

	public Trends getPlaceTrends(int arg0) throws TwitterException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseList<Location> getClosestTrends(GeoLocation arg0)
			throws TwitterException {
		// TODO Auto-generated method stub
		return null;
	}
}
