import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.HttpResponse;

/*
 * This class is used for the sentiment analysis of every tweet.
 * The proposed API is used (http://text-processing.com/docs/sentiment.html).
 * Process builder is used to convert the CURL request into a HTTP POST request, which returns the scores 
 * of the sentiment analysis of every tweet.
 * The handling of the results, which are retrieved from the InputStream are being processed with trivial 
 * algorithms.
 */

public class SentimentAnalysis {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	
	public String[] analyze(String text) throws IOException{

	        ProcessBuilder pb = new ProcessBuilder(
	                "curl",
	                 "-d","text=" + text,
	                "http://text-processing.com/api/sentiment/");

	       
	        pb.redirectErrorStream(true);
	        Process p = pb.start();

	        InputStream is = p.getInputStream();

	        	       
	        String s = convertStreamToString(is);

	        String neg = s.substring(s.indexOf("neg")+1);
	        neg = neg.replaceAll("\"","");
	        neg = neg.replaceAll("}", "");

	        String[] a = new String[4];
	        a = neg.split(",");	        
	        
	        String label = s.substring(s.indexOf("label"));


	        String[] values = new String[8];
	        String negative = a[0].substring(a[0].indexOf(":"));
	        String neutral = a[1].substring(a[1].indexOf(":"));
	        String positive = a[2].substring(a[2].indexOf(":"));
	        String resultLabel = a[3].substring(a[3].indexOf(":"));
	        String[] results = new String[a.length];
	        
	        results[0] = negative;
	        results[1] = neutral;
	        results[2] = positive;
	        results[3] = resultLabel;
	        
	        for(int i=0;i<results.length;i++)
	        	{
	        	results[i] = correctResults(results[i]);
	        	} 
	        
	        if (results[3] == null) System.out.println("post request returned null");
	        return results;

	    }

	public String correctResults(String res){
		String res1 = res;
		res1 = res1.replaceAll(":", "");
		res1 = res1.trim();
		return res1;
	}
	
	static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    static String extractResult(String whole){
        whole = whole.substring(whole.indexOf("{") );
        return whole;
    }

	    
	}


